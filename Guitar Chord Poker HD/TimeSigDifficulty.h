//
//  TimeSigDifficulty.h
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 10/06/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeSigDifficulty : UIImage
- (id)randomFourFourCard;
- (id)randomTwoFourCard;
- (id)randomThreeFourCard;
- (id)randomFiveFourCard;
- (id)randomSixEightCard;




@end
