//
//  MetronomeViewController.m
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 30/07/2013.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import "MetronomeViewController.h"

@implementation MetronomeViewController

@synthesize timer = _timer;
@synthesize meter = _meter;
@synthesize tempo = _tempo;

@synthesize button;
@synthesize meterLabel;
@synthesize tempoLabel;

- (void)setMeter:(NSNumber *)meter {
    _meter = meter;
    meterLabel.text = meter.stringValue;
    display.beats = meter;
}

- (void)setTempo:(NSNumber *)tempo {
    _tempo = tempo;
    tempoLabel.text = tempo.stringValue;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURL *tapSound = [[NSBundle mainBundle] URLForResource:@"tick" withExtension:@"aif"];

    // Store URL as a CFURLRef instance
    soundFileURLRef = (__bridge CFURLRef) tapSound;
    
    // Create a system sound object using the sound file
    AudioServicesCreateSystemSoundID(soundFileURLRef, &soundFileObject);
    
    // Initialize the display
    tempoLabel.text = [_tempo stringValue];
    meterLabel.text = [_meter stringValue];
    
    // Add long press gesture to stop metronome
    longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(stop:)];
    [self.view addGestureRecognizer:longPressGesture];
    
    // Set up beat display in same frame as button "Hidden"
    display = [[MetronomeBeatView alloc] initWithFrame:button.frame];
    display.hidden = YES;
    display.beats = _meter;
    
    // Add to view
    [self.view addSubview:display];
}

#pragma mark - Event handlers

- (void)stop:(id)senderOrNil {
    if (_timer) {
        // replace with start button
        display.hidden = YES;
        button.hidden = NO;
        
        // Clean
        [display reset];
        [_timer invalidate];
        _timer = nil;
    }
}

- (IBAction)start:(id)sender {
    if (!self.timer) {
        // Calculate the timer interval based on the tempo in beats per minute
        double interval = 60.0 / [_tempo doubleValue];
        button.hidden = YES;
        display.hidden = NO;
        
        // Start the repeating timer that counts the beats
        _timer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(tick:) userInfo:[NSNumber numberWithDouble:interval * ([_meter doubleValue] / 2)] repeats:YES];
    }
}

#pragma mark - NSTimer actions

- (void)tick:(NSTimer *)timer {
    // update the display
    [display tick:(NSNumber *)timer.userInfo];
    AudioServicesPlaySystemSound(soundFileObject);
}

#pragma mark - SettingsViewControllerDelegate

- (void)meterDidChange:(NSNumber *)meter {
    self.meter = meter;
}

- (void)tempoDidChange:(NSNumber *)tempo {
    self.tempo = tempo;
}


@end
