//
//  MetronomeBeatView.h
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 1/08/2013.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MetronomeBeatView : UIView {
    NSArray *ticks;
    NSInteger count;
}

@property (assign, nonatomic) NSNumber *beats;

- (void)tick:(NSNumber *)duration;
- (void)reset;

@end
