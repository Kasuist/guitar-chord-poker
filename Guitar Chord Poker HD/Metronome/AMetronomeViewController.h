//
//  MetronomeViewController.h
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 30/07/2013.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>

#import "MetronomeBeatView.h"


@interface AMetronomeViewController : UIViewController {
    UILongPressGestureRecognizer *longPressGesture;
    MetronomeBeatView *display;
    CFURLRef soundFileURLRef;
    SystemSoundID soundFileObject;
}

@property (assign) NSTimer *timer;
@property (strong, nonatomic) NSNumber *meter;
@property (strong, nonatomic) NSNumber *tempo;

@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UILabel *meterLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempoLabel;

- (IBAction)start:(id)sender;
- (void)stop:(id)senderOrNil;
- (void)tick:(NSTimer *)timer;

@end
