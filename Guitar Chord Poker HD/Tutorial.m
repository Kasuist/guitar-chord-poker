//
//  Tutorial.m
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 4/06/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import "Tutorial.h"

@implementation Tutorial

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
