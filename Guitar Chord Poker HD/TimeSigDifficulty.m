//
//  TimeSigDifficulty.m
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 10/06/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import "TimeSigDifficulty.h"

@implementation TimeSigDifficulty

#pragma mark - Time Signatures
- (id)randomFourFourCard {                                // ----- fourFour ---- //
    // search for difficulty setting
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/settings.plist", docDirectory];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    NSString *currentLevel = [dict objectForKey:@"difficultyLevel"];
    
    // Create Card
    UIImage *timeSignatureCard;
    
    // Check difficulty an init appropriate array
    if ([currentLevel isEqualToString:@"levelOne"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"fourFourOne.png"],
                                   [UIImage imageNamed:@"fourFourTwo.png"],
                                   [UIImage imageNamed:@"fourFourThree.png"],
                                   [UIImage imageNamed:@"fourFourFour.png"],
                                   [UIImage imageNamed:@"fourFourFive.png"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    } else if ([currentLevel isEqualToString:@"levelTwo"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"fourFourFour.png"],
                                   [UIImage imageNamed:@"fourFourFive.png"],
                                   [UIImage imageNamed:@"fourFourSix.png"],
                                   [UIImage imageNamed:@"fourFourSeven.png"],
                                   [UIImage imageNamed:@"fourFourEight.png"],
                                   [UIImage imageNamed:@"fourFourNine.png"],
                                   [UIImage imageNamed:@"fourFourTen.png"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    } else if ([currentLevel isEqualToString:@"levelThree"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"fourFourEight.png"],
                                   [UIImage imageNamed:@"fourFourNine.png"],
                                   [UIImage imageNamed:@"fourFourTen.png"],
                                   [UIImage imageNamed:@"fourFourEleven.png"],
                                   [UIImage imageNamed:@"fourFourTwelve.png"],
                                   [UIImage imageNamed:@"fourFourThirteen.png"],
                                   [UIImage imageNamed:@"fourFourFourteen.png"],
                                   [UIImage imageNamed:@"fourFourSixteen.png"],
                                   [UIImage imageNamed:@"fourFourSeventeen.png"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    } else if ([currentLevel isEqualToString:@"levelFour"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"fourFourEight.png"],
                                   [UIImage imageNamed:@"fourFourNine.png"],
                                   [UIImage imageNamed:@"fourFourTen.png"],
                                   [UIImage imageNamed:@"fourFourEleven.png"],
                                   [UIImage imageNamed:@"fourFourTwelve.png"],
                                   [UIImage imageNamed:@"fourFourThirteen.png"],
                                   [UIImage imageNamed:@"fourFourFourteen.png"],
                                   [UIImage imageNamed:@"fourFourSixteen.png"],
                                   [UIImage imageNamed:@"fourFourSeventeen.png"],
                                   [UIImage imageNamed:@"fourFourEighteen.png"],
                                   [UIImage imageNamed:@"fourFourNineteen.png"],
                                   [UIImage imageNamed:@"fourFourNineteen.png"],
                                   [UIImage imageNamed:@"fourFourTwenty.png"],
                                   [UIImage imageNamed:@"fourFourTwentyOne.png"],
                                   [UIImage imageNamed:@"fourFourTwentyTwo.png"],
                                   [UIImage imageNamed:@"fourFourTwentyThree.png"],
                                   [UIImage imageNamed:@"fourFourTwentyFour.png"],
                                   [UIImage imageNamed:@"fourFourTwentyFive.png"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    }
    return timeSignatureCard;
}

- (id)randomTwoFourCard {                            // ------- twoFour ------ //
    // search for difficulty setting
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/settings.plist", docDirectory];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    NSString *currentLevel = [dict objectForKey:@"difficultyLevel"];
    
    // Create Card
    UIImage *timeSignatureCard;
    
    // Check difficulty an init appropriate array
    if ([currentLevel isEqualToString:@"levelOne"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"twoFourSeven.png"],
                                   [UIImage imageNamed:@"twoFourEight.png"],
                                   [UIImage imageNamed:@"twoFourNine.png"],
                                   [UIImage imageNamed:@"twoFourOne.png"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    } else if ([currentLevel isEqualToString:@"levelTwo"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"twoFourSeven.png"],
                                   [UIImage imageNamed:@"twoFourEight.png"],
                                   [UIImage imageNamed:@"twoFourNine.png"],
                                   [UIImage imageNamed:@"twoFourOne.png"],
                                   [UIImage imageNamed:@"twoFourFive.png"],
                                   [UIImage imageNamed:@"twoFourSix.png"],
                                   [UIImage imageNamed:@"twoFourThree.png"],
                                   [UIImage imageNamed:@"twoFourTwo.png"],
                                   [UIImage imageNamed:@"twoFourFour.png"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    } else if ([currentLevel isEqualToString:@"levelThree"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"twoFourEight.png"],
                                   [UIImage imageNamed:@"twoFourNine.png"],
                                   [UIImage imageNamed:@"twoFourOne.png"],
                                   [UIImage imageNamed:@"twoFourFive.png"],
                                   [UIImage imageNamed:@"twoFourSix.png"],
                                   [UIImage imageNamed:@"twoFourThree.png"],
                                   [UIImage imageNamed:@"twoFourTwo.png"],
                                   [UIImage imageNamed:@"twoFourFour.png"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    } else if ([currentLevel isEqualToString:@"levelFour"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"twoFourNine.png"],
                                   [UIImage imageNamed:@"twoFourOne.png"],
                                   [UIImage imageNamed:@"twoFourFive.png"],
                                   [UIImage imageNamed:@"twoFourSix.png"],
                                   [UIImage imageNamed:@"twoFourThree.png"],
                                   [UIImage imageNamed:@"twoFourTwo.png"],
                                   [UIImage imageNamed:@"twoFourFour.png"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    }
    return timeSignatureCard;
}

- (id)randomThreeFourCard {                          // ------ threeFour ----- //
    // search for difficulty setting
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/settings.plist", docDirectory];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    NSString *currentLevel = [dict objectForKey:@"difficultyLevel"];
    
    // Create Card
    UIImage *timeSignatureCard;
    
    // Check difficulty an init appropriate array
    if ([currentLevel isEqualToString:@"levelOne"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"fourFourOne.png"],
                                   [UIImage imageNamed:@"fourFourTwo.png"],
                                   [UIImage imageNamed:@"fourFourThree.png"],
                                   [UIImage imageNamed:@"fourFourFour.png"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    } else if ([currentLevel isEqualToString:@"levelTwo"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"fourFourOne.png"],
                                   [UIImage imageNamed:@"fourFourTwo.png"],
                                   [UIImage imageNamed:@"fourFourThree.png"],
                                   [UIImage imageNamed:@"fourFourFour.png"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    } else if ([currentLevel isEqualToString:@"levelThree"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"fourFourOne.png"],
                                   [UIImage imageNamed:@"fourFourTwo.png"],
                                   [UIImage imageNamed:@"fourFourThree.png"],
                                   [UIImage imageNamed:@"fourFourFour.png"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    } else if ([currentLevel isEqualToString:@"levelFour"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"fourFourOne.png"],
                                   [UIImage imageNamed:@"fourFourTwo.png"],
                                   [UIImage imageNamed:@"fourFourThree.png"],
                                   [UIImage imageNamed:@"fourFourFour.png"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    }
    return timeSignatureCard;
}

- (id)randomFiveFourCard {                         // ----- fiveFour ------- //
    // search for difficulty setting
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/settings.plist", docDirectory];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    NSString *currentLevel = [dict objectForKey:@"difficultyLevel"];
    
    // Create Card
    UIImage *timeSignatureCard;
    
    // Check difficulty an init appropriate array
    if ([currentLevel isEqualToString:@"levelOne"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"fiveFourOne.png"],
                                   [UIImage imageNamed:@"fiveFourTwo.png"],
                                   [UIImage imageNamed:@"fiveFourThree.png"],
                                   [UIImage imageNamed:@"fiveFourFour.png"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    } else if ([currentLevel isEqualToString:@"levelTwo"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"fiveFourOne.png"],
                                   [UIImage imageNamed:@"fiveFourTwo.png"],
                                   [UIImage imageNamed:@"fiveFourThree.png"],
                                   [UIImage imageNamed:@"fiveFourFour.png"],
                                   [UIImage imageNamed:@"fiveFourFive.png"],
                                   [UIImage imageNamed:@"fiveFourSix.png"],
                                   [UIImage imageNamed:@"fiveFourSeven.png"],
                                   [UIImage imageNamed:@"fiveFourEight.png"],
                                   
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    } else if ([currentLevel isEqualToString:@"levelThree"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"fiveFourOne.png"],
                                   [UIImage imageNamed:@"fiveFourTwo.png"],
                                   [UIImage imageNamed:@"fiveFourThree.png"],
                                   [UIImage imageNamed:@"fiveFourFour.png"],
                                   [UIImage imageNamed:@"fiveFourFive.png"],
                                   [UIImage imageNamed:@"fiveFourSix.png"],
                                   [UIImage imageNamed:@"fiveFourSeven.png"],
                                   [UIImage imageNamed:@"fiveFourEight.png"],
                                   [UIImage imageNamed:@"fiveFourNine.png"],
                                   [UIImage imageNamed:@"fiveFourTen.png"],
                                   [UIImage imageNamed:@"fiveFourEleven"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    } else if ([currentLevel isEqualToString:@"levelFour"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"fiveFourThree.png"],
                                   [UIImage imageNamed:@"fiveFourFour.png"],
                                   [UIImage imageNamed:@"fiveFourFive.png"],
                                   [UIImage imageNamed:@"fiveFourSix.png"],
                                   [UIImage imageNamed:@"fiveFourSeven.png"],
                                   [UIImage imageNamed:@"fiveFourEight.png"],
                                   [UIImage imageNamed:@"fiveFourNine.png"],
                                   [UIImage imageNamed:@"fiveFourTen.png"],
                                   [UIImage imageNamed:@"fiveFourEleven"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    }
    return timeSignatureCard;
}

- (id)randomSixEightCard {                         // ----- sixEight ------- //
    // search for difficulty setting
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/settings.plist", docDirectory];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    NSString *currentLevel = [dict objectForKey:@"difficultyLevel"];
    
    // Create Card
    UIImage *timeSignatureCard;
    
    // Check difficulty an init appropriate array
    if ([currentLevel isEqualToString:@"levelOne"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"sixEightOne.png"],
                                   [UIImage imageNamed:@"sixEightTwo.png"],
                                   [UIImage imageNamed:@"sixEightThree.png"],
                                   [UIImage imageNamed:@"sixEightFour.png"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    } else if ([currentLevel isEqualToString:@"levelTwo"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"sixEightOne.png"],
                                   [UIImage imageNamed:@"sixEightTwo.png"],
                                   [UIImage imageNamed:@"sixEightThree.png"],
                                   [UIImage imageNamed:@"sixEightFour.png"],
                                   [UIImage imageNamed:@"sixEightFive.png"],
                                   [UIImage imageNamed:@"sixEightSix.png"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    } else if ([currentLevel isEqualToString:@"levelThree"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"sixEightThree.png"],
                                   [UIImage imageNamed:@"sixEightFour.png"],
                                   [UIImage imageNamed:@"sixEightFive.png"],
                                   [UIImage imageNamed:@"sixEightSix.png"],
                                   [UIImage imageNamed:@"sixEightSeven"],
                                   [UIImage imageNamed:@"sixEightEight"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    } else if ([currentLevel isEqualToString:@"levelFour"]) {
        // Create the array with time signatures
        NSArray *timeSignatures = [[NSArray alloc] initWithObjects:
                                   [UIImage imageNamed:@"sixEightThree.png"],
                                   [UIImage imageNamed:@"sixEightFour.png"],
                                   [UIImage imageNamed:@"sixEightFive.png"],
                                   [UIImage imageNamed:@"sixEightSix.png"],
                                   [UIImage imageNamed:@"sixEightSeven"],
                                   [UIImage imageNamed:@"sixEightEight"],
                                   [UIImage imageNamed:@"sixEightNine"],
                                   nil];
        // Choose random time signature
        NSUInteger randomIndex = arc4random() % [timeSignatures count];
        // assign the random time signature
        timeSignatureCard = [timeSignatures objectAtIndex:randomIndex];
        NSLog(@"Level One TimeSig GENERATED");
    }
    return timeSignatureCard;
}

@end
