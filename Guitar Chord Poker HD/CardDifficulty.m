//
//  CardDifficulty.m
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 10/06/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import "CardDifficulty.h"

@implementation CardDifficulty

#pragma mark - Playing Cards
- (id)randomBeginnerCard {
    UIImage *result;
    NSArray *threeStringCards = [[NSArray alloc] initWithObjects:
                                 [UIImage imageNamed:@"AMajor.png"],
                                 [UIImage imageNamed:@"CMajor.png"],
                                 [UIImage imageNamed:@"EMajor.png"],
                                 [UIImage imageNamed:@"DMajor.png"],
                                 [UIImage imageNamed:@"GMajor.png"],
                                 
                                 [UIImage imageNamed:@"EMinor.png"],
                                 [UIImage imageNamed:@"DMinor.png"],
                                 [UIImage imageNamed:@"AMinor.png"],

                                 nil];
    
    // Choose random card from array
    NSUInteger randomIndex = arc4random() % [threeStringCards count];
    // assign random card to "result"
    result = [threeStringCards objectAtIndex:randomIndex];
    NSLog(@"Level One Card GENERATED");
    
    // return that random card
    return result;
}

- (id)randomIntermediateCard {
    
    UIImage *result;
    // Add cards to the array
    NSArray *threeStringCards = [[NSArray alloc] initWithObjects:
                                 [UIImage imageNamed:@"AMajor.png"],
                                 [UIImage imageNamed:@"CMajor.png"],
                                 [UIImage imageNamed:@"EMajor.png"],
                                 [UIImage imageNamed:@"DMajor.png"],
                                 [UIImage imageNamed:@"GMajor.png"],
                                 
                                 [UIImage imageNamed:@"EMinor.png"],
                                 [UIImage imageNamed:@"DMinor.png"],
                                 [UIImage imageNamed:@"AMinor.png"],
                                 
                                 /////////
                                 [UIImage imageNamed:@"FMajor.png"],
                                 [UIImage imageNamed:@"BMajor.png"],
                                 
                                 [UIImage imageNamed:@"BMinor.png"],
                                 [UIImage imageNamed:@"FMinor.png"],
                                 [UIImage imageNamed:@"GMinor.png"],
                                 [UIImage imageNamed:@"FMinor.png"],

                                 [UIImage imageNamed:@"F7.png"],
                                 [UIImage imageNamed:@"E7.png"],
                                 [UIImage imageNamed:@"D7.png"],
                                 [UIImage imageNamed:@"A7.png"],
                                 [UIImage imageNamed:@"C7.png"],
                                 [UIImage imageNamed:@"G7.png"],
                                 [UIImage imageNamed:@"B7.png"],
                                 
                                 [UIImage imageNamed:@"EMaj7.png"],
                                 [UIImage imageNamed:@"AMaj7.png"],
                                 [UIImage imageNamed:@"DMaj7.png"],
                                 
                                 [UIImage imageNamed:@"Em7.png"],
                                 [UIImage imageNamed:@"Dm7.png"],
                                 [UIImage imageNamed:@"Am7.png"],
                                 
                                 nil];
    
    
    // Choose random card from array
    NSUInteger randomIndex = arc4random() % [threeStringCards count];
    // assign random card to "result"
    result = [threeStringCards objectAtIndex:randomIndex];
    NSLog(@"Level Two Card GENERATED");
    
    // return that random card
    return result;
}

- (id)randomAdvancedCard {
    
    UIImage *result;
    
    // Add cards to the array
    NSArray *threeStringCards = [[NSArray alloc] initWithObjects:
                                 [UIImage imageNamed:@"AMajor.png"],
                                 [UIImage imageNamed:@"CMajor.png"],
                                 [UIImage imageNamed:@"EMajor.png"],
                                 [UIImage imageNamed:@"DMajor.png"],
                                 [UIImage imageNamed:@"GMajor.png"],
                                 [UIImage imageNamed:@"FMajor.png"],
                                 [UIImage imageNamed:@"BMajor.png"],
                                 
                                 [UIImage imageNamed:@"EMinor.png"],
                                 [UIImage imageNamed:@"DMinor.png"],
                                 [UIImage imageNamed:@"AMinor.png"],
                                 [UIImage imageNamed:@"BMinor.png"],
                                 [UIImage imageNamed:@"FMinor.png"],
                                 [UIImage imageNamed:@"GMinor.png"],
                                 [UIImage imageNamed:@"FMinor.png"],
                                 
                                 [UIImage imageNamed:@"F7.png"],
                                 [UIImage imageNamed:@"E7.png"],
                                 [UIImage imageNamed:@"D7.png"],
                                 [UIImage imageNamed:@"A7.png"],
                                 [UIImage imageNamed:@"C7.png"],
                                 [UIImage imageNamed:@"G7.png"],
                                 [UIImage imageNamed:@"B7.png"],
                                 
                                 [UIImage imageNamed:@"EMaj7.png"],
                                 [UIImage imageNamed:@"AMaj7.png"],
                                 [UIImage imageNamed:@"DMaj7.png"],
                                 
                                 [UIImage imageNamed:@"Em7.png"],
                                 [UIImage imageNamed:@"Dm7.png"],
                                 [UIImage imageNamed:@"Am7.png"],
                                 
                                 /////////
                                 [UIImage imageNamed:@"Gm7.png"],
                                 [UIImage imageNamed:@"Cm7.png"],
                                 [UIImage imageNamed:@"Bm7.png"],
                                 [UIImage imageNamed:@"Fm7.png"],
                                 
                                 [UIImage imageNamed:@"BMaj7.png"],
                                 [UIImage imageNamed:@"CMaj7.png"],
                                 [UIImage imageNamed:@"FMaj7.png"],
                                 [UIImage imageNamed:@"GMaj7.png"],
                                 
                                 [UIImage imageNamed:@"Asus2.png"],
                                 [UIImage imageNamed:@"Bsus2.png"],
                                 [UIImage imageNamed:@"Csus2.png"],
                                 [UIImage imageNamed:@"Dsus2.png"],
                                 
                                 [UIImage imageNamed:@"Asus4.png"],
                                 [UIImage imageNamed:@"Bsus4.png"],
                                 [UIImage imageNamed:@"Csus4.png"],
                                 [UIImage imageNamed:@"Dsus4.png"],
                                 
                                 [UIImage imageNamed:@"A#Major.png"],
                                 [UIImage imageNamed:@"C#Major.png"],
                                 [UIImage imageNamed:@"D#Major.png"],
                                 [UIImage imageNamed:@"F#Major.png"],
                                 [UIImage imageNamed:@"G#Major.png"],
                                 
                                 [UIImage imageNamed:@"A#Minor.png"],
                                 [UIImage imageNamed:@"C#Minor.png"],
                                 [UIImage imageNamed:@"D#Minor.png"],
                                 [UIImage imageNamed:@"F#Minor.png"],
                                 [UIImage imageNamed:@"G#Minor.png"],
                                 
                                 [UIImage imageNamed:@"AbMajor.png"],
                                 [UIImage imageNamed:@"BbMajor.png"],
                                 [UIImage imageNamed:@"DbMajor.png"],
                                 [UIImage imageNamed:@"EbMajor.png"],
                                 [UIImage imageNamed:@"GbMajor.png"],
                                 
                                 [UIImage imageNamed:@"AbMinor.png"],
                                 [UIImage imageNamed:@"BbMinor.png"],
                                 [UIImage imageNamed:@"DbMinor.png"],
                                 [UIImage imageNamed:@"EbMinor.png"],
                                 [UIImage imageNamed:@"GbMinor.png"],
                                 
                                 nil];
    
    // Choose random card from array
    NSUInteger randomIndex = arc4random() % [threeStringCards count];
    // assign random card to "result"
    result = [threeStringCards objectAtIndex:randomIndex];
    NSLog(@"Level Three Card GENERATED");
    
    // return that random card
    return result;
}

- (id)randomProCard {
    
    UIImage *result;
    
    // Add cards to the array
    NSArray *threeStringCards = [[NSArray alloc] initWithObjects:
                                 // Major Natural
                                 [UIImage imageNamed:@"AMajor.png"],
                                 [UIImage imageNamed:@"CMajor.png"],
                                 [UIImage imageNamed:@"EMajor.png"],
                                 [UIImage imageNamed:@"DMajor.png"],
                                 [UIImage imageNamed:@"GMajor.png"],
                                 [UIImage imageNamed:@"FMajor.png"],
                                 [UIImage imageNamed:@"BMajor.png"],
                                 // Major sharp
                                 [UIImage imageNamed:@"A#Major.png"],
                                 [UIImage imageNamed:@"C#Major.png"],
                                 [UIImage imageNamed:@"D#Major.png"],
                                 [UIImage imageNamed:@"F#Major.png"],
                                 [UIImage imageNamed:@"G#Major.png"],
                                 // Major Flat
                                 [UIImage imageNamed:@"AbMajor.png"],
                                 [UIImage imageNamed:@"BbMajor.png"],
                                 [UIImage imageNamed:@"DbMajor.png"],
                                 [UIImage imageNamed:@"EbMajor.png"],
                                 [UIImage imageNamed:@"GbMajor.png"],

                                 
                                 // Minor Natural
                                 [UIImage imageNamed:@"EMinor.png"],
                                 [UIImage imageNamed:@"DMinor.png"],
                                 [UIImage imageNamed:@"AMinor.png"],
                                 [UIImage imageNamed:@"BMinor.png"],
                                 [UIImage imageNamed:@"FMinor.png"],
                                 [UIImage imageNamed:@"GMinor.png"],
                                 [UIImage imageNamed:@"FMinor.png"],
                                 // Minor sharp
                                 [UIImage imageNamed:@"A#Minor.png"],
                                 [UIImage imageNamed:@"C#Minor.png"],
                                 [UIImage imageNamed:@"D#Minor.png"],
                                 [UIImage imageNamed:@"F#Minor.png"],
                                 [UIImage imageNamed:@"G#Minor.png"],
                                 // Minro Flat
                                 [UIImage imageNamed:@"AbMinor.png"],
                                 [UIImage imageNamed:@"BbMinor.png"],
                                 [UIImage imageNamed:@"DbMinor.png"],
                                 [UIImage imageNamed:@"EbMinor.png"],
                                 [UIImage imageNamed:@"GbMinor.png"],
                                 
                                 
                                 // 7 Natural
                                 [UIImage imageNamed:@"F7.png"],
                                 [UIImage imageNamed:@"E7.png"],
                                 [UIImage imageNamed:@"D7.png"],
                                 [UIImage imageNamed:@"A7.png"],
                                 [UIImage imageNamed:@"C7.png"],
                                 [UIImage imageNamed:@"G7.png"],
                                 [UIImage imageNamed:@"B7.png"],
                                 // 7 Sharp
                                 [UIImage imageNamed:@"EMaj7.png"],
                                 [UIImage imageNamed:@"AMaj7.png"],
                                 [UIImage imageNamed:@"DMaj7.png"],
                                 [UIImage imageNamed:@"BMaj7.png"],
                                 [UIImage imageNamed:@"CMaj7.png"],
                                 [UIImage imageNamed:@"FMaj7.png"],
                                 [UIImage imageNamed:@"GMaj7.png"],
                                 
                                 [UIImage imageNamed:@"Em7.png"],
                                 [UIImage imageNamed:@"Dm7.png"],
                                 [UIImage imageNamed:@"Am7.png"],
                                 [UIImage imageNamed:@"Gm7.png"],
                                 [UIImage imageNamed:@"Cm7.png"],
                                 [UIImage imageNamed:@"Bm7.png"],
                                 [UIImage imageNamed:@"Fm7.png"],
                                 
                                 [UIImage imageNamed:@"Asus2.png"],
                                 [UIImage imageNamed:@"Bsus2.png"],
                                 [UIImage imageNamed:@"Csus2.png"],
                                 [UIImage imageNamed:@"Dsus2.png"],
                                 
                                 [UIImage imageNamed:@"Asus4.png"],
                                 [UIImage imageNamed:@"Bsus4.png"],
                                 [UIImage imageNamed:@"Csus4.png"],
                                 [UIImage imageNamed:@"Dsus4.png"],
                                
                                 /////////
                                 [UIImage imageNamed:@"Aaug.png"],
                                 [UIImage imageNamed:@"Baug.png"],
                                 [UIImage imageNamed:@"Caug.png"],
                                 [UIImage imageNamed:@"Daug.png"],
                                 [UIImage imageNamed:@"Eaug.png"],
                                 [UIImage imageNamed:@"Faug.png"],
                                 [UIImage imageNamed:@"Gaug.png"],
                                 
                                 [UIImage imageNamed:@"Adim.png"],
                                 [UIImage imageNamed:@"Bdim.png"],
                                 [UIImage imageNamed:@"Cdim.png"],
                                 [UIImage imageNamed:@"Ddim.png"],
                                 [UIImage imageNamed:@"Edim.png"],
                                 [UIImage imageNamed:@"Fdim.png"],
                                 [UIImage imageNamed:@"Gdim.png"],
                                 
                                 [UIImage imageNamed:@"AbMaj7.png"],
                                 [UIImage imageNamed:@"BbMaj7.png"],
                                 [UIImage imageNamed:@"DbMaj7.png"],
                                 [UIImage imageNamed:@"EbMaj7.png"],
                                 [UIImage imageNamed:@"GbMaj7.png"],
                                 
                                 [UIImage imageNamed:@"Abm7.png"],
                                 [UIImage imageNamed:@"Bbm7.png"],
                                 [UIImage imageNamed:@"Dbm7.png"],
                                 [UIImage imageNamed:@"Ebm7.png"],
                                 [UIImage imageNamed:@"Gbm7.png"],
                                 
                                 [UIImage imageNamed:@"A#Maj7.png"],
                                 [UIImage imageNamed:@"C#Maj7.png"],
                                 [UIImage imageNamed:@"D#Maj7.png"],
                                 [UIImage imageNamed:@"F#Maj7.png"],
                                 [UIImage imageNamed:@"G#Maj7.png"],
                                 
                                 [UIImage imageNamed:@"A#m7.png"],
                                 [UIImage imageNamed:@"C#m7.png"],
                                 [UIImage imageNamed:@"D#m7.png"],
                                 [UIImage imageNamed:@"F#m7.png"],
                                 [UIImage imageNamed:@"G#m7.png"],
                                 
                                 
                                 /// add few more exotic chords. 
                        
                                 nil];
    
    // Choose random card from array
    NSUInteger randomIndex = arc4random() % [threeStringCards count];
    // assign random card to "result"
    result = [threeStringCards objectAtIndex:randomIndex];
    NSLog(@"Level Four Card GENERATED");
    
    // return that random card
    return result;
}


#pragma mark - TEST METHOD FOR CHECKING KEY SIGNATURES

//- (id)randomBeginnerCard {
//    UIImage *result;
//    NSArray *threeStringCards;
//    
//    
//    
//    
//    //    search for current key setting. THIS NEEDS TO BE CHANGED TO DOCUMENT FILEPATH!!!
//    //    NSString *path = [[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"];
//    //    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
//    //    NSString *currentKey = [dict objectForKey:@"key"];
//    //
//    
//    //    Check what key is selected ------- remember to create more of these if statements.
//    //    This if statement should apply to other Random...Card methods.
//    
//    //    if ([currentKey isEqualToString:@"CMajor"]) {
//    //        // Add cards to the array
//    //        threeStringCards = [[NSArray alloc] initWithObjects:
//    //                            [UIImage imageNamed:@"GMajor.png"],
//    //                            [UIImage imageNamed:@"CMajor.png"],
//    //                            [UIImage imageNamed:@"AMinor.png"],
//    //                            [UIImage imageNamed:@"EMinor.png"],
//    //                            nil];
//    
//    //    WHAT NEEDS TO HAPPEN:
//    //    1. Key selection can be made inside the menu.
//    //
//    //    NOTE: Would love to have the key selection automatic based upon the first card. May need to create a subclass of CardView that will edit the key within the settings file and try that.
//    
//    // Choose random card from array
//    NSUInteger randomIndex = arc4random() % [threeStringCards count];
//    // assign random card to "result"
//    result = [threeStringCards objectAtIndex:randomIndex];
//    NSLog(@"Level One Card GENERATED");
//    
//    // return that random card
//    return result;
//}

@end
