//
//  CardView.h
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 10/04/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DifficultySelection.h"

@interface CardView : UIView

@property (assign, nonatomic, readonly) BOOL isFlipped;

- (id)initWithFrontImage:(UIImage *)frontImage backImage:(UIImage *)backImage;

@end
