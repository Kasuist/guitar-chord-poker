//
//  CardView.m
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 10/04/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import "CardView.h"
#import <QuartzCore/QuartzCore.h>
#import "PokerViewController.h"

@interface CardView ()
@property (assign, nonatomic, readwrite) BOOL isFlipped;

@property (strong, nonatomic) UIImageView *frontView;
@property (strong, nonatomic) UIImageView *backView;

- (void)addSwipeRecognizerForDirection:(UISwipeGestureRecognizerDirection)direction;

- (void)flipCard:(UIViewAnimationOptions)options;

- (void)isTapped:(UITapGestureRecognizer *)recognizer;
- (void)isSwiped:(UISwipeGestureRecognizer *)recognizer;

@end

@implementation CardView

@synthesize isFlipped;
@synthesize frontView, backView;

- (id)initWithFrontImage:(UIImage *)frontImage backImage:(UIImage *)backImage {
    CGRect rect = CGRectMake(0, 0, frontImage.size.width, frontImage.size.height);
    
    self = [super initWithFrame:rect];
    if (self) {
        self.isFlipped = NO;
        self.backgroundColor = [UIColor clearColor];
        
        // Set the Front View
        self.frontView = [[UIImageView alloc] initWithImage:frontImage];
        self.frontView.clipsToBounds = YES;
        self.frontView.layer.cornerRadius = 0.0;
        self.frontView.layer.borderWidth = 0.0;
        self.frontView.layer.borderColor = [[UIColor blackColor] CGColor];
        
        // Setup back view
        self.backView = [[UIImageView alloc] initWithImage:backImage];
        self.backView.clipsToBounds = YES;
        self.backView.layer.cornerRadius = 0.0;
        self.backView.layer.borderWidth = 0.0;
        self.backView.layer.borderColor = [[UIColor blackColor] CGColor];
        
        // Add Shadows
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.bounds];
        self.layer.masksToBounds = NO;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOffset = CGSizeMake(0.0f, 1.5f);
        self.layer.shadowOpacity = 0.5f;
        self.layer.shadowPath = shadowPath.CGPath;
        
        // Add both views
        [self addSubview:self.frontView];
        [self addSubview:self.backView];
        
        // Add gesture recognizers
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(isTapped:)]];
        
        [self addSwipeRecognizerForDirection:UISwipeGestureRecognizerDirectionLeft];
        [self addSwipeRecognizerForDirection:UISwipeGestureRecognizerDirectionRight];
        [self addSwipeRecognizerForDirection:UISwipeGestureRecognizerDirectionUp];
        [self addSwipeRecognizerForDirection:UISwipeGestureRecognizerDirectionDown];
    }
    return self;
}

- (void)addSwipeRecognizerForDirection:(UISwipeGestureRecognizerDirection)direction {
    // Create a swipe recognizer for the wanted direction
    UISwipeGestureRecognizer *swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(isSwiped:)];
    swipeRecognizer.direction = direction;
    [self addGestureRecognizer:swipeRecognizer];
}

#pragma mark - Gesture recognizer handlers
- (void)isTapped:(UITapGestureRecognizer *)recognizer {
    [self flipCard:UIViewAnimationOptionTransitionFlipFromLeft];
    
    if (isFlipped == NO) {
        NSLog(@"TAP! Back Image");
    } else {
        NSLog(@"TAP! Front Image");
        self.frontView.image = [[DifficultySelection alloc] ifDifficultyCard];
    }
}

- (void)isSwiped:(UISwipeGestureRecognizer *)recognizer {
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft) {
        [self flipCard:UIViewAnimationOptionTransitionFlipFromRight];
        if (isFlipped == NO) {
            NSLog(@"SWIPE LEFT! Back Image");
        } else {
            NSLog(@"SWIPE LEFT! Front Image");
            self.frontView.image = [[DifficultySelection alloc] ifDifficultyCard];
        }
        
    }
    else if (recognizer.direction == UISwipeGestureRecognizerDirectionRight) {
        [self flipCard:UIViewAnimationOptionTransitionFlipFromLeft];
        if (isFlipped == NO) {
            NSLog(@"SWIPE RIGHT! Back Image");
        } else {
            NSLog(@"SWIPE RIGHT!! Front Image");
            self.frontView.image = [[DifficultySelection alloc] ifDifficultyCard];
        }
    }
    else if (recognizer.direction == UISwipeGestureRecognizerDirectionUp) {
        [self flipCard:UIViewAnimationOptionTransitionFlipFromTop];
        if (isFlipped == NO) {
            NSLog(@"SWIPE UP! Back Image");
        } else {
            NSLog(@"SWIPE UP!! Front Image");
            self.frontView.image = [[DifficultySelection alloc] ifDifficultyCard];
        }
    }
    else if (recognizer.direction == UISwipeGestureRecognizerDirectionDown) {
        [self flipCard:UIViewAnimationOptionTransitionFlipFromBottom];
        if (isFlipped == NO) {
            NSLog(@"SWIPE DOWN! Back Image");
        } else {
            NSLog(@"SWIPE DOWN!! Front Image");
            self.frontView.image = [[DifficultySelection alloc] ifDifficultyCard];
        }
    }
}

#pragma mark - flip action
- (void)flipCard:(UIViewAnimationOptions)options {
    if (self.isFlipped == NO) {
        [UIView transitionFromView:self.backView toView:self.frontView duration:0.3 options:options completion:NULL];
        self.isFlipped = YES;
    }
    else if (self.isFlipped == YES) {
        [UIView transitionFromView:self.frontView toView:self.backView duration:0.3 options:options completion:NULL];
        self.isFlipped = NO;
    }
}
@end
