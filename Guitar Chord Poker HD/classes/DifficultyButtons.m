//
//  DifficultyButtons.m
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 4/06/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import "DifficultyButtons.h"

@implementation DifficultyButtons

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
        NSString *docDirectory = [sysPaths objectAtIndex:0];
        NSString *filePath = [NSString stringWithFormat:@"%@/settings.plist", docDirectory];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
        NSString *currentLevel = [dict objectForKey:@"difficultyLevel"];
        
        // Add buttons to view
        [self addGradeOneButton];
        [self addGradeTwoButton];
        [self addGradeThreeButton];
        [self addGradeFourButton];
        
        // set selected state
        if ([currentLevel isEqualToString:@"levelOne"]) {
            self.gradeOneButton.selected = YES; // Set first button as selected
        } else if ([currentLevel isEqualToString:@"levelTwo"]) {
            self.gradeTwoButton.selected = YES;
        } else if ([currentLevel isEqualToString:@"levelThree"]) {
            self.gradeThreeButton.selected = YES;
        } else {
            self.gradeFourButton.selected = YES;
        }
    }
    return self;
}

#pragma mark - Button Customisation

- (void)addGradeOneButton {
    // Create button
    self.gradeOneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.gradeOneButton.frame = CGRectMake(0, 0, 260, 44); // This will change button size and position
    //[self.gradeOneButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Regular" size:18]];  //// Thicker text
    
    // Set button appearance for Normal state
    [self.gradeOneButton setTitle:@"Beginner" forState:UIControlStateNormal];
    [self.gradeOneButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.gradeOneButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.0]] forState:UIControlStateNormal];
    
    // Set button appearance for Selected state
    [self.gradeOneButton setTitle:@"Beginner" forState:UIControlStateSelected];
    [self.gradeOneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [self.gradeOneButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:0.0/255.0 green:190.0/255.0 blue:165.0/255.0 alpha:0.9]] forState:UIControlStateSelected];

    // execute state change method
    [self.gradeOneButton addTarget:self action:@selector(changeGradeOneState:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.gradeOneButton];
}

- (void)addGradeTwoButton {
    // Create button
    self.gradeTwoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.gradeTwoButton.frame = CGRectMake(0, 44, 260, 44); // This will change button size and position
    
    // Set button appearance for Normal state
    [self.gradeTwoButton setTitle:@"Intermediate" forState:UIControlStateNormal];
    [self.gradeTwoButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.gradeTwoButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.0]] forState:UIControlStateNormal];
    
    // Set button appearance for Selected state
    [self.gradeTwoButton setTitle:@"Intermediate" forState:UIControlStateSelected];
    [self.gradeTwoButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [self.gradeTwoButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:0.0/255.0 green:190.0/255.0 blue:165.0/255.0 alpha:0.9]] forState:UIControlStateSelected];
    
    [self.gradeTwoButton addTarget:self action:@selector(changeGradeTwoState:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.gradeTwoButton];
}

- (void)addGradeThreeButton {
    // Create button
    self.gradeThreeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.gradeThreeButton.frame = CGRectMake(0, 88, 260, 44); // This will change button size and position
    
    // Set button appearance for Normal state
    [self.gradeThreeButton setTitle:@"Advanced" forState:UIControlStateNormal];
    [self.gradeThreeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.gradeThreeButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.0]] forState:UIControlStateNormal];
    
    // Set button appearance for Selected state
    [self.gradeThreeButton setTitle:@"Advanced" forState:UIControlStateSelected];
    [self.gradeThreeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [self.gradeThreeButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:0.0/255.0 green:190.0/255.0 blue:165.0/255.0 alpha:0.9]] forState:UIControlStateSelected];
    
    // execute state change method
    [self.gradeThreeButton addTarget:self action:@selector(changeGradeThreeState:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.gradeThreeButton];
}

- (void)addGradeFourButton {
    // Create button
    self.gradeFourButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.gradeFourButton.frame = CGRectMake(0, 132, 260, 44); // This will change button size and position
    
    // Set button appearance for Normal state
    [self.gradeFourButton setTitle:@"Pro" forState:UIControlStateNormal];
    [self.gradeFourButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.gradeFourButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.0]] forState:UIControlStateNormal];
    
    // Set button appearance for Selected state
    [self.gradeFourButton setTitle:@"Pro" forState:UIControlStateSelected];
    [self.gradeFourButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [self.gradeFourButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:0.0/255.0 green:190.0/255.0 blue:165.0/255.0 alpha:0.9]] forState:UIControlStateSelected];
    
    // execute state change method
    [self.gradeFourButton addTarget:self action:@selector(changeGradeFourState:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.gradeFourButton];
}

#pragma mark - Button state changes
- (void)changeGradeOneState:(id)sender {
    // Change selection state
    NSLog(@"Grade One Button Tapped");
    self.gradeOneButton.selected = YES;
    
    // search for dificulty setting
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/settings.plist", docDirectory];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    // set difficulty
    NSString *newNumber = @"levelOne";
    [dict setObject:newNumber forKey:@"difficultyLevel"];
    [dict writeToFile:filePath atomically:YES];
    NSString *currentLevel = [dict objectForKey:@"difficultyLevel"];
    NSLog(@"%@", currentLevel);
    
    // remove selection state from other buttons
    self.gradeTwoButton.selected = NO;
    self.gradeThreeButton.selected = NO;
    self.gradeFourButton.selected = NO;
}

- (void)changeGradeTwoState:(id)sender {
    // Change selection state
    NSLog(@"Grade Two Button Tapped");
    self.gradeTwoButton.selected = YES;
    
    // search for dificulty setting
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/settings.plist", docDirectory];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    // set difficulty
    NSString *newNumber = @"levelTwo";
    [dict setObject:newNumber forKey:@"difficultyLevel"];
    [dict writeToFile:filePath atomically:YES];
    NSString *currentLevel = [dict objectForKey:@"difficultyLevel"];
    NSLog(@"%@", currentLevel);
    
    // Remove select state from other buttons
    self.gradeOneButton.selected = NO;
    self.gradeThreeButton.selected = NO;
    self.gradeFourButton.selected = NO;
}

- (void)changeGradeThreeState:(id)sender {
    // Change selection state
    NSLog(@"Grade Three Button Tapped");
    self.gradeThreeButton.selected = YES;
    
    // search for dificulty setting
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/settings.plist", docDirectory];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    // set difficulty
    NSString *newNumber = @"levelThree";
    [dict setObject:newNumber forKey:@"difficultyLevel"];
    [dict writeToFile:filePath atomically:YES];
    NSString *currentLevel = [dict objectForKey:@"difficultyLevel"];
    NSLog(@"%@", currentLevel);
    
    // remove selection state from other buttons
    self.gradeOneButton.selected = NO;
    self.gradeTwoButton.selected = NO;
    self.gradeFourButton.selected = NO;
}

- (void)changeGradeFourState:(id)sender {
    // Change selection state
    NSLog(@"Grade Four Button Tapped");
    self.gradeFourButton.selected = YES;
    
    // search for dificulty setting
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/settings.plist", docDirectory];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    // set difficulty
    NSString *newNumber = @"levelFour";
    [dict setObject:newNumber forKey:@"difficultyLevel"];
    [dict writeToFile:filePath atomically:YES];
    NSString *currentLevel = [dict objectForKey:@"difficultyLevel"];
    NSLog(@"%@", currentLevel);

    
    // remove selection state from other buttons
    self.gradeOneButton.selected = NO;
    self.gradeTwoButton.selected = NO;
    self.gradeThreeButton.selected = NO;
}

#pragma mark - Image with color
- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
