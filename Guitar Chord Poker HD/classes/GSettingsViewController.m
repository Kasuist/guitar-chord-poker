//
//  SettingsViewController.m
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 5/06/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import "GSettingsViewController.h"

@interface GSettingsViewController () 
@end

@implementation GSettingsViewController {
    UIScrollView *scrollView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Sets the popover background color
        self.view.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // method for setting up and drawing the scroll view in which the buttons will be drawn
    [self addScrollView];    
    
    // Initialize buttons from respective classes
    DifficultyButtons *buttons = [[DifficultyButtons alloc] initWithFrame:CGRectMake(0, 0, 260, 176)];
    TimeSignatureButtons *timeSignatures = [[TimeSignatureButtons alloc] initWithFrame:CGRectMake(0, 200, 260, 176)];
    
    // Draw buttons to the scroll view
    [scrollView addSubview:buttons];
    [scrollView addSubview:timeSignatures];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addScrollView {
    scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    scrollView.contentSize = CGSizeMake(260, 300);
    scrollView.alwaysBounceVertical = YES;
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [scrollView setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:scrollView];
}

@end
