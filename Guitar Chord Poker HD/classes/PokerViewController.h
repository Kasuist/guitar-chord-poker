//
//  PokerViewController.h
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 10/04/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "DifficultySelection.h"
#import "CardView.h"
#import "TimeSignatures.h"
#import "MetronomeViewController.h"
#import "DifficultyButtons.h"
#import "SettingsButton.h"
#import "GSettingsViewController.h"

@interface PokerViewController : UIViewController <UIPopoverControllerDelegate, UIScrollViewDelegate>

@property (nonatomic, retain) UIView *view;
@property (nonatomic, strong) SettingsButton *settingsButton;
@property (nonatomic, strong) UIPopoverController *popUpController;
@property (nonatomic, strong) MetronomeViewController *metronome;
@property (nonatomic, strong) SettingsViewController *settings;
@property (nonatomic, strong) UIScrollView *scrollView;

// Create gesture recognizer on entire view (self.gesture) to redraw cards after one big swipe, within bounds of cards, but cant activate anything else.

@end
