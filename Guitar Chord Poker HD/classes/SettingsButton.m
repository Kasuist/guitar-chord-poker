//
//  SettingsButton.m
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 5/06/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import "SettingsButton.h"

@implementation SettingsButton

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.settingsButtonImage = [UIImage imageNamed:@"settings.png"];
        
        [self setImage:self.settingsButtonImage forState:UIControlStateNormal];
        
    }
    return self;
}

+ (id)addSettingsButton {
    return [[self alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
}

@end
