//
//  TimeSignatureButtons.h
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 10/06/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeSignatureButtons : UIView

@property (nonatomic, strong) UIButton *fourFourButton;
@property (nonatomic, strong) UIButton *twoFourButton;
@property (nonatomic, strong) UIButton *threeFourButton;
@property (nonatomic, strong) UIButton *fiveFourButton;
@property (nonatomic, strong) UIButton *sixEightButton;

- (id)initWithFrame:(CGRect)frame;

@end
