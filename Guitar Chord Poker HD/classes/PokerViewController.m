//
//  PokerViewController.m
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 10/04/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import "PokerViewController.h"

@implementation PokerViewController

@synthesize metronome, scrollView, settings;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Set Background color
    [self.view setBackgroundColor: [self colorWithHexString:@"FFFFFF"]];
    
    // Add everything to the view
	[self addPlayingCardAtX:155 y:570];
    [self addPlayingCardAtX:395 y:570];
    [self addPlayingCardAtX:635 y:570];
    [self addPlayingCardAtX:875 y:570];
    
    [self addTimeSignatureAtX:512 y:300];
    [self addSettingsButtonAtX:997 y:42];
    
    [self addMetronomeView];
    
    // Setup the PopUpController and it's content
    GSettingsViewController *settingsView = [[GSettingsViewController alloc] init];
    self.popUpController = [[UIPopoverController alloc] initWithContentViewController:settingsView];
    self.popUpController.popoverContentSize = CGSizeMake(260, 300);
    
    // Move settings plist into Documents Directory
    NSFileManager *fileManger = [NSFileManager defaultManager];
    NSError *error;
    NSArray *pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    
    NSString *documentDirectoryPath = [pathsArray objectAtIndex:0];
    
    NSString *destinationPath= [documentDirectoryPath stringByAppendingPathComponent:@"settings.plist"];
    
    NSLog(@"plist path %@", destinationPath);
    if ([fileManger fileExistsAtPath:destinationPath]){
        NSLog(@"database location %@", destinationPath);
        return;
    }
    NSString *sourcePath=[[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:@"settings.plist"];
                          
                          [fileManger copyItemAtPath:sourcePath toPath:destinationPath error:&error];
}

- (void)addMetronomeView {
    // Setup bounding box for metronome and settings to display
    CGRect rect;
    rect.origin.x = 210;   //// Set X position of Metronome view
    rect.origin.y = 30;    //// Set Y position of Metronome view
    rect.size.height = 150.0;
    rect.size.width = 600.0;
    
    self.wantsFullScreenLayout = YES;

    // Configure the scrollView
    self.scrollView = [[UIScrollView alloc] initWithFrame:rect];
    self.scrollView.contentSize = CGSizeMake(rect.size.width, 2 * rect.size.height);
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.delegate = self;
    
    // defaults, must add user settings.
    NSNumber *defaultMeter = [NSNumber numberWithInt:4];
    NSNumber *defaultTempo = [NSNumber numberWithInt:120];
    
    // Set up the metronome view
    metronome = [[MetronomeViewController alloc] initWithNibName:@"MetronomeView" bundle:nil];
    metronome.view.frame = CGRectMake(0.0, metronome.view.frame.size.height, metronome.view.frame.size.width, metronome.view.frame.size.height);
    metronome.meter = defaultMeter;
    metronome.tempo = defaultTempo;
    
    [scrollView scrollRectToVisible:metronome.view.frame animated:NO];
    
    [scrollView addSubview:metronome.view];
    
    // Set up the settings view
    settings = [[SettingsViewController alloc] initWithNibName:@"SettingsView" bundle:nil];
    settings.meter = defaultMeter;
    settings.tempo = defaultTempo;
    settings.settingsDelegate = metronome;
    
    [scrollView addSubview:settings.view];
    
    [self.view addSubview:scrollView];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    // Stop the metronome if we scroll.
    [self.metronome stop:nil];
}

#pragma mark - Navigation Controller Delegate

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    CGSize size = self.view.bounds.size;
    
    // Set the content size of the scroll view to allow for scrolling between the metronome and settings only when the settings view is displayed by the navigation controller. Otherwise we're drilled down in the settings and should set the content height to match the view bounds so that users can't scroll until they go back to the root settings view.
    if ([viewController isEqual:settings]) {
        size.height *= 2;
    }
    
    self.scrollView.contentSize = size;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Implementation Methods
- (void)addPlayingCardAtX:(CGFloat)x y:(CGFloat)y {
    DifficultySelection *ca = [[DifficultySelection alloc] ifDifficultyCard];
    UIImage *back = [UIImage imageNamed:@"cardBack.png"];
    
    // create the view that will display the card images
    CardView *playingCard = [[CardView alloc] initWithFrontImage:ca backImage:back];
    
    // Set X and Y frame origin -- Centered
    CGRect f = playingCard.frame;
    f.origin.x = x-(f.size.width/2.0);
    f.origin.y = y-(f.size.height/2.0);
    playingCard.frame = f;
    
    // Add to view
    [self.view addSubview:playingCard];
}

- (void)addTimeSignatureAtX:(CGFloat)x y:(CGFloat)y {
    DifficultySelection *timeSignatureCardFront = [[DifficultySelection alloc] ifDifficultyTimeSig];
    UIImage *timeSignatureCardBack = [UIImage imageNamed:@"timeBack.png"];
    
    // create the view that will display the card images
    TimeSignatures *signatureCard = [[TimeSignatures alloc] initTimeSignatureWithFrontImage:timeSignatureCardFront backImage:timeSignatureCardBack];
    
    // Set X and Y frame origin -- Centered
    CGRect f = signatureCard.frame;
    f.origin.x = x-(f.size.width/2.0);
    f.origin.y = y-(f.size.height/2.0);
    signatureCard.frame = f;
    
    // Add to view
    [self.view addSubview:signatureCard];
}

- (void)addSettingsButtonAtX:(CGFloat)x y:(CGFloat)y {
    self.settingsButton = [SettingsButton addSettingsButton];
    
    // Set X and Y frame origin -- Centered
    CGRect f = self.settingsButton.frame;
    f.origin.x = x-(f.size.width/2.0);
    f.origin.y = y-(f.size.height/2.0);
    self.settingsButton.frame = f;
    
    [self.settingsButton addTarget:self action:@selector(showPopUp:) forControlEvents:UIControlEventTouchUpInside];
    
    // Add to View
    [self.view addSubview:self.settingsButton];
}

#pragma mark - PopOver Action
- (void)showPopUp:(id)sender {
    // Show the popover from the button that was tapped.
    [self.popUpController presentPopoverFromRect:self.settingsButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark - Color Hex Method
-(UIColor*)colorWithHexString:(NSString*)hex {
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

@end


