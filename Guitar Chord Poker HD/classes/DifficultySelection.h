//
//  DifficultySelection.h
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 11/04/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// The below classes might be used later on to make the app more modular.
// For now though, all methods are inside the .m of this class
#import "CardDifficulty.h"
#import "TimeSigDifficulty.h"

@interface DifficultySelection : UIImage

@property (nonatomic, strong) CardDifficulty *getCard;
@property (nonatomic, strong) TimeSigDifficulty *getTimeSig;

- (id)ifDifficultyTimeSig;
- (id)ifDifficultyCard;

@end
