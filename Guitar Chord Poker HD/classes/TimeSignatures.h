//
//  TimeSignatures.h
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 18/04/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DifficultySelection.h"

@interface TimeSignatures : UIView

@property (assign, nonatomic, readonly) BOOL isFlipped;

- (id)initTimeSignatureWithFrontImage:(UIImage *)frontImage backImage:(UIImage *)backImage;

@end
