//
//  SettingsButton.h
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 5/06/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsButton : UIButton

@property (nonatomic, strong) UIImage *settingsButtonImage;

- (id)initWithFrame:(CGRect)frame;

+ (id)addSettingsButton;

@end
