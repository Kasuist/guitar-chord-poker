//
//  SettingsViewController.h
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 5/06/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DifficultyButtons.h"
#import "TimeSignatureButtons.h"

@interface GSettingsViewController : UIViewController

@property (nonatomic, strong) DifficultyButtons *difficultyButtons;
@property (nonatomic, strong) TimeSignatureButtons *timeSignatureButtons;

@end
