//
//  PokerAppDelegate.h
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 10/04/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PokerViewController.h"

@interface PokerAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
