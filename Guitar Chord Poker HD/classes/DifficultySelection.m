//
//  DifficultySelection.m
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 11/04/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import "DifficultySelection.h"

@implementation DifficultySelection

#pragma mark - Difficulty Control
- (id)ifDifficultyCard {
    
    UIImage *card;
    
    // search for difficulty setting
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/settings.plist", docDirectory];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    NSString *currentLevel = [dict objectForKey:@"difficultyLevel"];
    
    // change outcome depending on difficulty
    if ([currentLevel isEqualToString:@"levelOne"]) {
        card = [[CardDifficulty alloc] randomBeginnerCard];
    }
    else if ([currentLevel isEqualToString:@"levelTwo"]) {
        card = [[CardDifficulty alloc] randomIntermediateCard];
    }
    else if ([currentLevel isEqualToString:@"levelThree"]) {
        card = [[CardDifficulty alloc] randomAdvancedCard];
    }
    else if ([currentLevel isEqualToString:@"levelFour"]){
        card = [[CardDifficulty alloc] randomProCard];
    }
    return card;
}

- (id)ifDifficultyTimeSig {
    
    UIImage *timeSignature;
    
    // search for signature setting
    NSArray *sigPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *sigDirectory = [sigPaths objectAtIndex:0];
    NSString *signaturePath = [NSString stringWithFormat:@"%@/settings.plist", sigDirectory];
    NSMutableDictionary *sig = [[NSMutableDictionary alloc] initWithContentsOfFile:signaturePath];
    NSString *currentSignature = [sig objectForKey:@"timeSignature"];
    
    // change outcome depending on signature selection
    if ([currentSignature isEqualToString:@"fourFour"]) {
        timeSignature = [[TimeSigDifficulty alloc] randomFourFourCard];
    }
    else if ([currentSignature isEqualToString:@"twoFour"]) {
        timeSignature = [[TimeSigDifficulty alloc] randomTwoFourCard];
    }
    else if ([currentSignature isEqualToString:@"threeFour"]) {
        timeSignature = [[TimeSigDifficulty alloc] randomThreeFourCard];
    }
    else if ([currentSignature isEqualToString:@"fiveFour"]) {
        timeSignature = [[TimeSigDifficulty alloc] randomFiveFourCard];
    }
    else if ([currentSignature isEqualToString:@"sixEight"]) {
        timeSignature = [[TimeSigDifficulty alloc] randomSixEightCard];
    }

    return timeSignature;
}
@end
