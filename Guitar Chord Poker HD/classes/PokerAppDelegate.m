//
//  PokerAppDelegate.m
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 10/04/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import "PokerAppDelegate.h"


@implementation PokerAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UIViewController *viewController = [[PokerViewController alloc] init];
    self.window.rootViewController = viewController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
