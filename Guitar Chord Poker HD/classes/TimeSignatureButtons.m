//
//  TimeSignatureButtons.m
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 10/06/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import "TimeSignatureButtons.h"

@implementation TimeSignatureButtons

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
        NSString *docDirectory = [sysPaths objectAtIndex:0];
        NSString *filePath = [NSString stringWithFormat:@"%@/settings.plist", docDirectory];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
        NSString *currentLevel = [dict objectForKey:@"timeSignature"];

        // Add buttons to the view
        [self addFourFourButton];
        [self addTwoFourButton];
        [self addThreeFourButton];
        [self addFiveFourButton];
        [self addSixEightButton];
        
        // set selected state
        if ([currentLevel isEqualToString:@"fourFour"]) {
            self.fourFourButton.selected = YES; // Set first button as selected
        } else if ([currentLevel isEqualToString:@"twoFour"]) {
            self.twoFourButton.selected = YES;
        } else if ([currentLevel isEqualToString:@"threeFour"]) {
            self.threeFourButton.selected = YES;
        } else  if ([currentLevel isEqualToString:@"fiveFour"]){
            self.fiveFourButton.selected = YES;
        } else {
            self.sixEightButton.selected = YES;
        }
    }
    return self;
}

#pragma mark - Button Customisation
- (void)addTwoFourButton {
    // Create button
    self.twoFourButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.twoFourButton.frame = CGRectMake(8, 0, 44, 44); // This will change button size and position
    self.twoFourButton.layer.cornerRadius = self.twoFourButton.frame.size.height / 2;
    self.twoFourButton.clipsToBounds = YES;
    
    self.twoFourButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.twoFourButton.titleLabel.numberOfLines = 2;
    [self.twoFourButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    // Set button appearance for Normal state
    [self.twoFourButton setTitle:@"2\n4" forState:UIControlStateNormal];
    [self.twoFourButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.twoFourButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.0]] forState:UIControlStateNormal];
    
    // Set button appearance for Selected state
    [self.twoFourButton setTitle:@"2\n4" forState:UIControlStateSelected];
    [self.twoFourButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [self.twoFourButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:0.0/255.0 green:190.0/255.0 blue:165.0/255.0 alpha:0.9]] forState:UIControlStateSelected];
    
    [self.twoFourButton addTarget:self action:@selector(changeTwoFourState:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.twoFourButton];
}

- (void)addThreeFourButton {
    // Create button
    self.threeFourButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.threeFourButton.frame = CGRectMake(58, 0, 44, 44); // This will change button size and position
    self.threeFourButton.layer.cornerRadius = self.threeFourButton.frame.size.height / 2;
    self.threeFourButton.clipsToBounds = YES;
    
    self.threeFourButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.threeFourButton.titleLabel.numberOfLines = 2;
    [self.threeFourButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    // Set button appearance for Normal state
    [self.threeFourButton setTitle:@"3\n4" forState:UIControlStateNormal];
    [self.threeFourButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.threeFourButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.0]] forState:UIControlStateNormal];
    
    // Set button appearance for Selected state
    [self.threeFourButton setTitle:@"3\n4" forState:UIControlStateSelected];
    [self.threeFourButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [self.threeFourButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:0.0/255.0 green:190.0/255.0 blue:165.0/255.0 alpha:0.9]] forState:UIControlStateSelected];
    
    // execute state change method
    [self.threeFourButton addTarget:self action:@selector(changeThreeFourState:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.threeFourButton];
}
- (void)addFourFourButton {
    // Create button
    self.fourFourButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.fourFourButton.frame = CGRectMake(108, 0, 44, 44); // This will change button size and position
    self.fourFourButton.layer.cornerRadius = self.fourFourButton.frame.size.height / 2;
    self.fourFourButton.clipsToBounds = YES;
    
    self.fourFourButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.fourFourButton.titleLabel.numberOfLines = 2;
    [self.fourFourButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    // Set button appearance for Normal state
    [self.fourFourButton setTitle:@"4\n4" forState:UIControlStateNormal];
    [self.fourFourButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.fourFourButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.0]] forState:UIControlStateNormal];
    
    // Set button appearance for Selected state
    [self.fourFourButton setTitle:@"4\n4" forState:UIControlStateSelected];
    [self.fourFourButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [self.fourFourButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:0.0/255.0 green:190.0/255.0 blue:165.0/255.0 alpha:0.9]] forState:UIControlStateSelected];
    
    // execute state change method
    [self.fourFourButton addTarget:self action:@selector(changeFourFourState:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.fourFourButton];
}

- (void)addFiveFourButton {
    // Create button
    self.fiveFourButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.fiveFourButton.frame = CGRectMake(158, 0, 44, 44); // This will change button size and position
    self.fiveFourButton.layer.cornerRadius = self.fiveFourButton.frame.size.height / 2;
    self.fiveFourButton.clipsToBounds = YES;
    
    self.fiveFourButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.fiveFourButton.titleLabel.numberOfLines = 2;
    [self.fiveFourButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    // Set button appearance for Normal state
    [self.fiveFourButton setTitle:@"5\n4" forState:UIControlStateNormal];
    [self.fiveFourButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.fiveFourButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.0]] forState:UIControlStateNormal];
    
    // Set button appearance for Selected state
    [self.fiveFourButton setTitle:@"5\n4" forState:UIControlStateSelected];
    [self.fiveFourButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [self.fiveFourButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:0.0/255.0 green:190.0/255.0 blue:165.0/255.0 alpha:0.9]] forState:UIControlStateSelected];
    
    // execute state change method
    [self.fiveFourButton addTarget:self action:@selector(changeFiveFourState:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.fiveFourButton];
}
- (void)addSixEightButton {
    // Create button
    self.sixEightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sixEightButton.frame = CGRectMake(208, 0, 44, 44); // This will change button size and position
    self.sixEightButton.layer.cornerRadius = self.sixEightButton.frame.size.height / 2;
    self.sixEightButton.clipsToBounds = YES;
    
    self.sixEightButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.sixEightButton.titleLabel.numberOfLines = 2;
    [self.sixEightButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    // Set button appearance for Normal state
    [self.sixEightButton setTitle:@"6\n8" forState:UIControlStateNormal];
    [self.sixEightButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.sixEightButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.0]] forState:UIControlStateNormal];
    
    // Set button appearance for Selected state
    [self.sixEightButton setTitle:@"6\n8" forState:UIControlStateSelected];
    [self.sixEightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [self.sixEightButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:0.0/255.0 green:190.0/255.0 blue:165.0/255.0 alpha:0.9]] forState:UIControlStateSelected];
    
    // execute state change method
    [self.sixEightButton addTarget:self action:@selector(changeSixEightState:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.sixEightButton];
}

#pragma mark - Button state changes
- (void)changeFourFourState:(id)sender {
    // Change selection state
    NSLog(@"4/4 button tapped");
    self.fourFourButton.selected = YES;
    
    // search for dificulty setting
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/settings.plist", docDirectory];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    // set new difficulty
    NSString *newSignature = @"fourFour";
    [dict setObject:newSignature forKey:@"timeSignature"];
    [dict writeToFile:filePath atomically:YES];
    
    // show new difficulty in console
    NSString *currentLevel = [dict objectForKey:@"timeSignature"];
    NSLog(@"Current Level: %@", currentLevel);
    
    // remove selection state from other buttons
    self.twoFourButton.selected = NO;
    self.threeFourButton.selected = NO;
    self.fiveFourButton.selected = NO;
    self.sixEightButton.selected = NO;
}

- (void)changeTwoFourState:(id)sender {
    // Change selection state
    NSLog(@"2/4 button tapped");
    self.twoFourButton.selected = YES;
    
    // search for dificulty setting
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/settings.plist", docDirectory];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    // set new difficulty
    NSString *newSignature = @"twoFour";
    [dict setObject:newSignature forKey:@"timeSignature"];
    [dict writeToFile:filePath atomically:YES];
    
    // show new difficulty in console
    NSString *currentLevel = [dict objectForKey:@"timeSignature"];
    NSLog(@"Current Level: %@", currentLevel);
    
    // Remove select state from other buttons
    self.fourFourButton.selected = NO;
    self.threeFourButton.selected = NO;
    self.fiveFourButton.selected = NO;
    self.sixEightButton.selected = NO;
}

- (void)changeThreeFourState:(id)sender {
    // Change selection state
    NSLog(@"3/4 button tapped");
    self.threeFourButton.selected = YES;
    
    // search for dificulty setting
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/settings.plist", docDirectory];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    // set new difficulty
    NSString *newSignature = @"threeFour";
    [dict setObject:newSignature forKey:@"timeSignature"];
    [dict writeToFile:filePath atomically:YES];
    
    // show new difficulty in console
    NSString *currentLevel = [dict objectForKey:@"timeSignature"];
    NSLog(@"Current Level: %@", currentLevel);
    
    // remove selection state from other buttons
    self.fourFourButton.selected = NO;
    self.twoFourButton.selected = NO;
    self.fiveFourButton.selected = NO;
    self.sixEightButton.selected = NO;
}

- (void)changeFiveFourState:(id)sender {
    // Change selection state
    NSLog(@"5/4 button tapped");
    self.fiveFourButton.selected = YES;
    
    // search for dificulty setting
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/settings.plist", docDirectory];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    // set new difficulty
    NSString *newSignature = @"fiveFour";
    [dict setObject:newSignature forKey:@"timeSignature"];
    [dict writeToFile:filePath atomically:YES];
    
    // show new difficulty in console
    NSString *currentLevel = [dict objectForKey:@"timeSignature"];
    NSLog(@"Current Level: %@", currentLevel);
    
    // remove selection state from other buttons
    self.fourFourButton.selected = NO;
    self.twoFourButton.selected = NO;
    self.threeFourButton.selected = NO;
    self.sixEightButton.selected = NO;

}

- (void)changeSixEightState:(id)sender {
    // Change selection state
    NSLog(@"6/8 button tapped");
    self.sixEightButton.selected = YES;
    
    // search for dificulty setting
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString *docDirectory = [sysPaths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/settings.plist", docDirectory];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    
    // set new difficulty
    NSString *newSignature = @"sixEight";
    [dict setObject:newSignature forKey:@"timeSignature"];
    [dict writeToFile:filePath atomically:YES];
    
    // show new difficulty in console
    NSString *currentLevel = [dict objectForKey:@"timeSignature"];
    NSLog(@"Current Level: %@", currentLevel);
    
    // remove selection state from other buttons
    self.fourFourButton.selected = NO;
    self.twoFourButton.selected = NO;
    self.threeFourButton.selected = NO;
    self.fiveFourButton.selected = NO;
}

#pragma mark - Image with color
- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
@end
