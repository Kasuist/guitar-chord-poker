//
//  DifficultyButtons.h
//  Guitar Chord Poker HD
//
//  Created by Beau Young on 4/06/13.
//  Copyright (c) 2013 Rondo Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DifficultyButtons : UIView

@property (nonatomic, strong) UIButton *gradeOneButton;
@property (nonatomic, strong) UIButton *gradeTwoButton;
@property (nonatomic, strong) UIButton *gradeThreeButton;
@property (nonatomic, strong) UIButton *gradeFourButton;

- (id)initWithFrame:(CGRect)frame;

@end
